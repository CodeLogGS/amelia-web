var x_vals = [];
var y_vals = [];
var a, b, c, d;
var optimizer;
var drawing = false;
var learn = false;

function setup() {

  createCanvas(600, 600);

  a = tf.variable(tf.scalar(random(-1, 1)));
  b = tf.variable(tf.scalar(random(-1, 1)));
  c = tf.variable(tf.scalar(random(-1, 1)));
  d = tf.variable(tf.scalar(random(-1, 1)));

  optimizer = tf.train.adam(0.1);
  console.log("ax^3 + bx^2 + cx + d");

}

function loss(pred, labels) {
  return pred.sub(labels).square().mean();
}

function mousePressed() {
  drawing = true;
}

function mouseReleased() {
  drawing = false;
}

function keyPressed() {
  learn = true;
}

function predict(x) {
  const xs = tf.tensor1d(x);

  // y = ax^3 + bx^2 + cx + d
  const ys = xs.pow(tf.scalar(3)).mul(a)
    .add(xs.square().mul(b))
    .add(xs.mul(c))
    .add(d);

  return ys;
}

function draw() {

  background(0);

  stroke(255);
  strokeWeight(8);

  for (var i = 0; i < x_vals.length; i++) {
    var px = map(x_vals[i], -1, 1, 0, width);
    var py = map(y_vals[i], -1, 1, 0, height);

    point(px, py);
  }

  if (drawing) {

    x_vals.push(map(mouseX, 0, width, -1, 1));
    y_vals.push(map(mouseY, 0, height, -1, 1));

  } else if (learn) {

    stroke(255);
    strokeWeight(2);
    tf.tidy(() => {
      const ys = tf.tensor1d(y_vals);
      optimizer.minimize(() => loss(predict(x_vals), ys));

      beginShape();
      noFill();
      for (var i = -1; i < 1.01; i += 0.05) {

        var xs = [i];
        var ty = predict(xs);
        var yf = ty.dataSync();
        ty.dispose();

        var sx = map(xs[0], -1, 1, 0, width);
        var sy = map(yf[0], -1, 1, 0, height);

        vertex(sx, sy);
      }
      endShape();

    });
  }
}
