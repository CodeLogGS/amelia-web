## v0.0.1 -- 2018-06-18

* Added a test neural network for solving the XOR problem.

## v0.1.0 -- 2018-06-20

* Added polynomial regression using the Adam optimizer. (Stochastic gradient descent)

## v0.1.1 -- 2018-06-21

* Patch to add some descriptive notes.
* Fixed:  Regression learning starting before user finished inputting data.

## v0.2.0 -- 2018-06-23

* Added a webcam image classifier (not working a.t.m.).

## v0.2.1 -- 2018-06-24

* Tried my best to get the webcam classifier to work, but alas, it refuses. If anyone else has any suggestions, please file an issue.
