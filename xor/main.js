var resolution = 10;
var cols;
var data = [];
var inputs = [];
var model, fData;
var xs, ys;
var train_xs, train_ys;
var trainReady = true;
var dataReady = true;

function setup() {

  createCanvas(600, 600);
  cols = width / resolution;

  for (var i = 0; i < cols; i++) {
    data.push(0);
    inputs.push(new Array(cols));
  }

  // Create training data
  for (var i = 0; i < cols; i++) {
    for (var j = 0; j < cols; j++) {

      data[i][j] = random(1);
      inputs[i][j] = [map(i, 0, cols - 1, 0, 1), map(j, 0, cols - 1, 0, 1)];

    }
  }

  model = tf.sequential();

// Build the network.
  var hidden = tf.layers.dense({
    inputShape: [2],
    units: 4,
    activation: 'softsign'
  });

  var dropout = tf.layers.dropout({
    rate: 0.2
  });

  var output = tf.layers.dense({
    units: 1,
    useBias: true
  });

  model.add(hidden);
  //model.add(dropout);
  model.add(output);

  var optimizer = tf.train.adam(0.01);
  model.compile({
    optimizer: optimizer,
    loss: tf.losses.meanSquaredError
  });

  train_xs = tf.tensor2d([
    [0, 0],
    [1, 0],
    [0, 1],
    [1, 1]
  ]);

  train_ys = tf.tensor2d([
    [0],
    [1],
    [1],
    [0]
  ]);

  setInterval(updateData, 10);

}

function trainModel() {
  return model.fit(train_xs, train_ys, {
    shuffle: true,
    epoch: 10
  });
}

function updateData() {

  tf.tidy(() => {

    var inp = [];
    for (var i = 0; i < cols; i++) {
      inp = inp.concat(inputs[i]);
    }

    var in_t = tf.tensor2d(inp);
    data = model.predict(in_t).dataSync();

  });

}

function draw() {

  background(0);

  if (trainReady) {
    trainReady = false;
    trainModel().then((response) => {
      trainReady = true;
      document.getElementById("loss").value = response.history.loss[0];
      //console.log(response.history.loss[0]);
    });
  }

  // Draw the outputs
  var index = 0;
  for (var i = 0; i < cols; i++) {
    for (var j = 0; j < cols; j++) {

      if (data[index] == null)
        continue;

      bright = floor(map(data[index], 0, 1, 0, 255));
      fill(bright);
      noStroke();
      rect(i * resolution, j * resolution, i * resolution + resolution, j * resolution + resolution);

      index++;
    }
  }

  //noLoop();

}
