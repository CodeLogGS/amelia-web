function toGrayscale(ps) {

  var nps = [];

  for (var i = 0; i < ps.length; i += 4) {

    var ave = 0;
    ave += ps[i];
    ave += ps[i + 1];
    ave += ps[i + 2];
    ave += ps[i + 3];
    ave = floor(ave / 4);

    nps.push(ave);

  }

  return nps;

}

function downSample(ps, factor) {

  var nps = [];

  factor = floor(factor ** 2);

  for (var i = 0; i < ps.length; i += factor) {

    var ave = 0;
    for (var j = 0; j < factor; j++)
      ave += ps[i + j];

    ave = floor(ave / factor);
    nps.push(ave);

  }

  return nps;

}
