var selectedClass = 0;
var camera;
var imgs = [];
var tLabels = [];
var imgCount = 0;

var model, optimizer;

// KeyCodes
var Enter = 13;
var b = 66;
var t = 84;
var left = 37;
var right = 39;
var p = 80;

function setup() {

  createCanvas(200, 200);

  camera = new Camera(document.getElementById("cam"));
  camera.startFeed();

  buildModel();

}

function draw() {

  var img = document.getElementById('canv').getContext('2d').getImageData(0, 0, 200, 200).data;

  loadPixels();

  for (var i = 0; i < pixels.length - 3; i += 4) {

    pixels[i] = img[i];
    pixels[i + 1] = img[i + 1];
    pixels[i + 2] = img[i + 2];
    pixels[i + 3] = img[i + 3];

  }

  updatePixels();

  textSize(20);
  fill(255);
  text("Selected class: " + selectedClass, 0, 20);

}

function keyPressed() {

  if (keyCode == Enter) {

    var snap = camera.takeSnapshot(document.getElementById("canv"));
    var graySnap = toGrayscale(snap)
    imgs.push(downSample(graySnap, 4));

    var temp = [];
    for (var i = 0; i < 4; i++) {
      if (i == selectedClass)
        temp.push(1);
      else
        temp.push(0);
    }

    tLabels.push(temp);

    imgCount++;

  } else if (keyCode == b) {

    buildModel();

  } else if (keyCode == t) {

    trainModel();

  } else if (keyCode == right) {

    selectedClass = (selectedClass + 1) % 4;

  } else if (keyCode == left) {

    selectedClass--;
    if (selectedClass < 0)
      selectedClass += 4;

  } else if (keyCode == p) {

    predict();

  }

  //console.log(keyCode);

}

function predict() {

  var img = camera.takeSnapshot(document.getElementById('canv'));
  img = downSample(toGrayscale(img), 4);

  const data = tf.tensor2d([img]);
  const results = model.predict(data.reshape([1, 50, 50, 1]));
  results.print();

  data.dispose();
  results.dispose();

}

function trainModel() {

  const data = tf.tensor2d(imgs);
  const labels = tf.tensor2d(tLabels);

  labels.print();

  model.fit(data.reshape([imgCount, 50, 50, 1]), labels, {
    epochs: 100,
    shuffle: true
  })
  .then((response) => {
    console.log(response.history.loss[0]);
    data.dispose();
    labels.dispose();
  })
  .catch((error) => console.log(error));

}

function buildModel() {

  //const c = numClasses;
  model = tf.sequential();

  // Layers: conv2d -> conv2d -> dense -> output

  model.add(tf.layers.conv2d({
    inputShape: [50, 50, 1],
    filters: 5,
    kernelSize: 3,
    activation: 'relu'
  }));
  model.add(tf.layers.conv2d({
    filters: 5,
    kernelSize: 3,
    activation: 'relu'
  }));

  model.add(tf.layers.flatten());
  model.add(tf.layers.dense({
    units: 64,
    activation: 'relu'
  }));
  model.add(tf.layers.dense({
    units: 4,
    activation: 'softmax'
  }));

  optimizer = tf.train.sgd(0.15);

  model.compile({
    optimizer: optimizer,
    loss: 'categoricalCrossentropy',
    metrics: ['accuracy']
  });

}
