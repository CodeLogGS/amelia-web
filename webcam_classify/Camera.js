function Camera(video) {

  this.video = video;

  this.startFeed = () => {

    navigator.mediaDevices.getUserMedia({video: true}).then((stream) => {
      video.srcObject = stream;
      video.onloadedmetadata = () => video.play();
    });

  }

  this.takeSnapshot = (tmpCanvas) => {
    var ctx = tmpCanvas.getContext('2d');
    ctx.drawImage(video, 0, 0, this.video.width, this.video.height);
    var ps = ctx.getImageData(0, 0, this.video.width, this.video.height).data;

    return ps;
  }

}
